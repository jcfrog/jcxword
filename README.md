## Objectif

Faire un script de jeu de grilles (mots fléchés, mots croisés, sudoku...) le plus simple possible, sans base de données, sans node.js ou php, juste du html et du javascript pour pouvoir le mettre sur n'importe quel hébergement, ou même sans serveur. On veut pouvoir :

- jouer en ligne ou pas (mode jeu)
- créer des grilles (mode édition)

Les données d'une grille sont fournies dans un fichier js avec principalement la solution et les définitions. Le mode **édition** permet de créer ces fichiers simplement.

Pas de compte, pas de protection, nous sommes entre gens de bonne compagnie.

Exemple de (petite) grille 

![Exemple de grille](doc/grille.png)

## Demo


### Jouer
- [Jouer la grille "Geekeries"](https://jcfrog.com/jcxword/?id=geekeries), mots fléchés
- [Jouer la grille "kids0"](https://jcfrog.com/jcxword/?id=kids0), mots entrelacés
- [Jouer la grille "kids1"](https://jcfrog.com/jcxword/?id=kids1), mots fléchés
- [Jouer la grille "klaire"](https://jcfrog.com/jcxword/?id=klaire), mots fléchés
- [Jouer la grille "klaire2"](https://jcfrog.com/jcxword/?id=klaire2), mots fléchés
- [Jouer la grille "metal"](https://jcfrog.com/jcxword/?id=metal), mots fléchés
- [Jouer la grille "de gauche"](https://jcfrog.com/jcxword/?id=goch1), mots fléchés
- [Jouer la grille "de droite"](https://jcfrog.com/jcxword/?id=droite1), mots fléchés

### Editer
La grille "Geekeries" en mode **édition** ( ⚠️ attention, divulgache la solution... ⚠️ ) : [édition grille "Geekeries"](https://jcfrog.com/jcxword/?id=geekeries&edit=y)


## Ce qui marche

- mode jeu (mots fléchés, mots entrecroisés)
- mode édition via [paramètres d'URL](https://framagit.org/jcfrog/jcxword/-/wikis/jcxword-wiki#param%C3%A8tres-durl)

## A faire

- version mobile
- mode jeu mots croisés
- interfaces non geeks pour gestion de grilles (création, sauvegardes, mises en ligne...)
- gestion impression
- gestion zoom/pan

## Documentation

Voir le [wiki](https://framagit.org/jcfrog/jcxword/-/wikis/jcxword-wiki).

## Licence

GPLv3

## Illustrations

Exemples de grilles de mots fléchés :

Grille "Geekeries", [à jouer ici](https://jcfrog.com/jcxword/?id=geekeries).

![](doc/grille-geekeries.png)

Grille de [Klaire](https://klaire.fr/) qui m'a donnée l'idée de faire ce script, [à jouer ici](https://jcfrog.com/jcxword/?id=klaire).

![](doc/grille-klaire.png)

Exemple de grille en mode **edition**

![Exemple de grille en mode édition](doc/grille-edit.png)
