
var bNotations = false; // cells coordinates display
var editMode = false;

window.mobileAndTabletCheck = function () {
    let check = false;
    (function (a) { if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino|android|ipad|playbook|silk/i.test(a) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0, 4))) check = true; })(navigator.userAgent || navigator.vendor || window.opera);
    return check;
};
var mobileMode = window.mobileAndTabletCheck();

const urlParams = new URLSearchParams(window.location.search);

if (deb = urlParams.get("debug")) {
    if (deb == "y") bNotations = true;
}
if (mob = urlParams.get("mobile")) {
    if (mob == "y") mobileMode = true;
}



// size of cells, everything is computed from this
var cellsz = 100;
if (newCellsz = urlParams.get("sz")) {
    cellsz = parseInt(newCellsz);
}
var cfg = {
    w: cellsz,
    h: cellsz,
    borderLineWidth: 3,
    interCellLinesWidth: 2,
    bordersColor: "orange"
}
if (id = urlParams.get("id")){
    document.querySelector('meta[property="og:image"]').setAttribute("content", "cards/"+id+".jpg");
    document.querySelector('meta[name="twitter:image"]').setAttribute("content", "cards/"+id+".jpg");
}

// board data
// will be loaded once the game id is chosen
var sol = []; // solution 
var defCells = {}; // definitions
var board = { // default values to be overridden 
    c: 8,
    l: 8,
    W: 8 * cfg.w,
    H: 8 * cfg.h,
}

var cells = []; // current user input
var ui = { x: -1, y: -1 }; // current user input prompt coordinates 
var lastInput = [-1, -1];
var dirInput = [1, 0]; // vertical or horizontal user typing




// history function
var stateIdx = -1;
var nbStates = 100;
var states = [];
function addStepToHistory() {
    if (states.length == nbStates) {
        stateIdx--;
        states.shift();
    }
    stateIdx++;
    states[stateIdx] = stringifyBoard();
    // clean if necessary
    while ((stateIdx + 1) < states.length) {
        states.pop();
    }
}
function undo() {
    if (stateIdx >= 0) {
        loadStringifiedBoard(states[stateIdx]);
        stateIdx--;
    }
}
function redo() {
    if (stateIdx < (states.length - 1)) {
        stateIdx++;
        loadStringifiedBoard(states[stateIdx]);
    }
}
function stringifyBoard() {
    var backup = {
        board: board,
        sol: sol,
        defCells: defCells,
        cells: cells
    }
    return JSON.stringify(backup);
}
function loadStringifiedBoard(blob) {
    var obj = JSON.parse(blob);

    if (obj.board) board = obj.board;
    /*if (obj.sol) board = obj.sol ;
    if (obj.defCells) defCells = obj.defCells ;*/
    if (obj.cells) cells = obj.cells;
    if (obj.ui) {
        ui.x = obj.ui.x;
        ui.y = obj.ui.y;
    }


    redrawBackground();
    redrawInput();
    if (editMode) {
        inputCellInit(ui.x, ui.y);
    }

    //console.log(obj);
}





function wrapText(context, text, x, y, maxWidth, maxHeight, lineHeight) {
    // started from https://codepen.io/nishiohirokazu/pen/jjNyye

    context.save();

    context.font = Math.floor(cfg.w / 90 * 14) + "px defsfont";
    context.textBaseline = "top";

    var words = text.split(' ');
    var line = '';
    var lines = [];

    var bounds = { x: x, y: y, w: maxWidth, h: 0 };

    var curY = y;
    for (var n = 0; n < words.length; n++) {
        var testLine = line + words[n] + ' ';
        var metrics = context.measureText(testLine);
        var testWidth = metrics.width;
        if (testWidth > maxWidth && n > 0) {
            lines.push(line.trim());
            //context.fillText(line, x, curY);
            line = words[n] + ' ';
            curY += lineHeight;
            bounds.h = curY - bounds.y;
        }
        else {
            line = testLine;
        }
    }

    //context.fillText(line, x, curY);
    lines.push(line.trim());
    bounds.h += lineHeight;

    /*console.log("lines",lines);
    context.beginPath();
    context.strokeStyle="blue";   
    context.lineWidth="2";   
    context.rect(bounds.x,bounds.y+(maxHeight-bounds.h)/2,bounds.w,bounds.h);
    context.stroke();*/

    curY = y + (maxHeight - bounds.h) / 2;
    for (var n = 0; n < lines.length; n++) {
        var metrics = context.measureText(lines[n]);
        context.fillText(lines[n], x + (maxWidth - metrics.width) / 2, curY);
        curY += lineHeight;
    }

    context.restore();
}


function drawTarget(ctx, x, y, dir) {
    var sz = 10;
    ctx.save();
    ctx.fillStyle = cfg.bordersColor;
    ctx.translate(x, y);
    if (dir == "W") ctx.rotate(Math.PI);
    if (dir == "S") ctx.rotate(Math.PI / 2);
    if (dir == "N") ctx.rotate(-Math.PI / 2);
    ctx.beginPath();
    ctx.moveTo(0, 0);
    ctx.lineTo(0, -sz);
    ctx.lineTo(sz, 0);
    ctx.lineTo(0, sz);
    ctx.lineTo(0, 0);
    ctx.fill();
    ctx.restore();
}

function txtCell(ctx, txt, c, l, target, split, prctStart, prctStop) {
    var margin = 0.05; // in %
    var r = {
        x: cfg.w * c,
        y: cfg.h * l,
        w: cfg.w,
        h: cfg.h
    }
    switch (split) {
        default: // "F" = Full
            break;
        case "H": // horizontal slice
            r.y = r.y + cfg.h * prctStart / 100;
            r.h = cfg.h * (prctStop - prctStart) / 100;
            break;
        case "V": // vertical slice
            r.x = r.x + cfg.w * prctStart / 100;
            r.w = cfg.w * (prctStop - prctStart) / 100;
            break;
    }
    wrapText(ctx, txt,
        r.x + margin / 2 * cfg.w,
        r.y + margin * cfg.h,
        r.w - 2 * margin / 2 * cfg.w,
        r.h - 2 * margin * cfg.h,
        Math.floor(cfg.w / 90 * 14));

    // draw dirs
    var m = target.match(/^(\d+)-(\d+)-([NEWS])$/);
    var c2 = parseInt(m[1]);
    var l2 = parseInt(m[2]);
    var dir = m[3];
    if ((c - c2) < 0) drawTarget(ctx, r.x + r.w, r.y + r.h / 2, dir);
    if ((c - c2) > 0) drawTarget(ctx, r.x, r.y + r.h / 2, dir);
    if ((l - l2) < 0) drawTarget(ctx, r.x + r.w / 2, r.y + r.h, dir);
    if ((l - l2) > 0) drawTarget(ctx, r.x + r.w / 2, r.y, dir);

    //console.log(c,l,pos,target);
}

function drawNotations(ctx) {
    ctx.save();

    ctx.textBaseline = "top";
    ctx.font = "10px";
    ctx.fillStyle = "grey";

    for (var c = 0; c < board.c; c++) {
        for (var l = 0; l < board.l; l++) {
            ctx.fillText(c + '-' + l, c * cfg.w + 5, l * cfg.h + 5);
        }
    }

    ctx.restore();
}
function redrawBackground(ctx) {

    var dispGrid = true;

    if (typeof xtraInfos !== 'undefined') {
        if (typeof xtraInfos.gridType !== 'undefined') {
            dispGrid = (xtraInfos.gridType !== "intersect");
        }
    }

    if (ctx === undefined) {
        var cnv = $("#board-bg")[0];
        ctx = cnv.getContext('2d');
    }

    ctx.save();

    ctx.fillStyle = "white";
    ctx.strokeStyle = cfg.bordersColor;
    ctx.lineWidth = cfg.borderLineWidth;


    ctx.fillRect(0, 0, board.W, board.H);

    if (dispGrid) {
        // draw black cells
        for (var c = 0; c < board.c; c++) {
            for (var l = 0; l < board.l; l++) {
                if (sol[c][l] == "*") {
                    ctx.fillStyle = "black";
                    ctx.fillRect(c * cfg.w, l * cfg.h, cfg.w, cfg.h);
                }
            }
        }
        ctx.beginPath();
        ctx.moveTo(0, 0);
        ctx.lineTo(board.W, 0);
        ctx.lineTo(board.W, board.H);
        ctx.lineTo(0, board.H);
        ctx.lineTo(0, 0);
        ctx.closePath();
        ctx.stroke();

        ctx.lineWidth = cfg.interCellLinesWidth;
        for (var i = 0; i < board.c; i++) {
            ctx.beginPath();
            ctx.moveTo(i * cfg.w, 0);
            ctx.lineTo(i * cfg.w, board.H);
            ctx.closePath();
            ctx.stroke();
        }
        for (var i = 0; i < board.l; i++) {
            ctx.beginPath();
            ctx.moveTo(0, i * cfg.h);
            ctx.lineTo(board.W, i * cfg.h);
            ctx.closePath();
            ctx.stroke();
        }
    } else {
        // no grid, no black cell
        for (var c = 0; c < board.c; c++) {
            for (var l = 0; l < board.l; l++) {
                // draw letters and def cells
                var t = cellGetStatus(c, l);
                switch (t) {
                    default:
                        break;
                    case CS_DEFS:
                    case CS_LETTER:
                        ctx.lineWidth = cfg.interCellLinesWidth;
                        //ctx.fillRect(c * cfg.w, l * cfg.h, cfg.w, cfg.h);
                        ctx.beginPath();
                        ctx.moveTo(c * cfg.w, l * cfg.h);
                        ctx.lineTo((c + 1) * cfg.w, l * cfg.h);
                        ctx.lineTo((c + 1) * cfg.w, (l + 1) * cfg.h);
                        ctx.lineTo(c * cfg.w, (l + 1) * cfg.h);
                        ctx.lineTo(c * cfg.w, l * cfg.h);
                        ctx.closePath();
                        ctx.stroke();
                        break;
                }
            }
        }
    }



    ctx.restore();

    redrawDefs(ctx);

    if (bNotations) drawNotations(ctx);

}

function redrawInput(ctx) {

    if (ctx === undefined) {
        var cnvInput = $("#board-input")[0];
        cnvInput.width = board.W;
        cnvInput.height = board.H;
        ctx = cnvInput.getContext('2d');
    }


    if ((ui.x >= 0) && (ui.y >= 0)) {
        ctx.save();

        if (editMode) {
            ctx.fillStyle = "rgba(255,150,0,.3)";
        } else {
            ctx.fillStyle = "rgba(0,255,150,.3)";
        }
        ctx.translate(ui.x * cfg.w, ui.y * cfg.h);

        ctx.fillRect(0, 0, cfg.w, cfg.h);

        var sz = 10;
        ctx.fillStyle = "white";
        if (dirInput[0] > 0) {
            ctx.translate(cfg.w - sz, cfg.h / 2);
        } else {
            ctx.translate(cfg.w / 2, cfg.h - sz);
            ctx.rotate(Math.PI / 2)
        }
        ctx.beginPath();
        ctx.moveTo(0, 0); ctx.lineTo(0, -sz); ctx.lineTo(sz, 0); ctx.lineTo(0, sz); ctx.lineTo(0, 0);
        ctx.fill();
        ctx.closePath();


        ctx.restore();
    }

    redrawLetters(ctx);

}

function drawLetter(letter, c, l, ctx) {

    ctx.save();

    ctx.translate(c * cfg.w, l * cfg.h);

    ctx.font = Math.floor(cfg.w / 90 * 50) + "px gridfont";
    ctx.textAlign = "center";
    ctx.textBaseline = "middle";
    ctx.fillText(letter, cfg.w / 2, cfg.h / 2);

    ctx.restore();

}

function redrawLetters(ctx) {

    ctx.save();

    var score = checkScore();

    if ((score == 100) && (!editMode)) {
        ctx.fillStyle = "orange";
    } else {
        ctx.fillStyle = "black";
    }
    for (var c = 0; c < board.c; c++) {
        for (var l = 0; l < board.l; l++) {
            var char = cells[c][l];
            if (char != "?") {
                drawLetter(char, c, l, ctx);
            }
        }
    }
    if (!editMode){
        $("#score").text(score+"%");
    }

    ctx.restore();
}

function drawSplitter(ctx, split, c, l, prct) {
    ctx.save();

    ctx.lineWidth = 2;
    ctx.strokeStyle = cfg.bordersColor;
    ctx.beginPath();
    if (split == "V") {// vertical split
        ctx.moveTo(cfg.w * (c + prct / 100), cfg.h * l);
        ctx.lineTo(cfg.w * (c + prct / 100), cfg.h * (l + 1));
    } else { // horizontal split
        ctx.moveTo(cfg.w * c, cfg.h * (l + prct / 100));
        ctx.lineTo(cfg.w * (c + 1), cfg.h * (l + prct / 100));
    }
    ctx.closePath();
    ctx.stroke();

    ctx.restore();
}



function redrawDefs(ctx) {

    for (const d in defCells) {
        var item = defCells[d];
        var m = d.match(/^(\d+)-(\d+)$/);
        var c = parseInt(m[1]);
        var l = parseInt(m[2]);

        var split = item.dir || "F";

        var defaultHeighPrct = 100 / item.defs.length;

        var offset = 0;
        for (var i = 0; i < item.defs.length; i++) {
            var def = item.defs[i];
            txtCell(ctx, def.text, c, l, def.target, split, offset, offset + (def.space || defaultHeighPrct));
            if (i > 0) {
                // draw the interline
                drawSplitter(ctx, split, c, l, offset);
            }
            offset += (def.space || defaultHeighPrct);
        }
    }

}

// cells status 
const CS_UNKOWN = -1;
const CS_BLACK = 0;
const CS_LETTER = 1;
const CS_DEFS = 2;
var stylesLegends = [];
stylesLegends[CS_BLACK] = "Noire";
stylesLegends[CS_LETTER] = "Lettre";
stylesLegends[CS_DEFS] = "Définitions";

// def cells split dir
const CSD_FULL = "F";
const CSD_VERT = "V";
const CSD_HORZ = "H";
var dirLegends = [];
dirLegends[CSD_FULL] = "Aucun";
dirLegends[CSD_VERT] = "Vertical";
dirLegends[CSD_HORZ] = "Horizontal";

function cellGetStatus(c, l) {
    var status = CS_UNKOWN;
    if ((c >= 0) && (l >= 0) && (c < board.c) && (l < board.l)) {
        if (sol[c][l] == "*") status = CS_BLACK;
        else if (sol[c][l] == "?") status = CS_DEFS;
        else status = CS_LETTER;
    }
    return status;
}

function cellKey(c, l) { return c + "-" + l; }

function setDefDir(c, l, dir) {
    defCells[cellKey(c, l)].dir = dir;
    redrawBackground();
    inputCellInit(c, l);
}

function getDirDivs(dir) {
    if (dir == "N") return "<div class='dir-but dir-but-N side-N'></div><div class='dir-but dir-but-E side-N'></div>";
    if (dir == "E") return "<div class='dir-but dir-but-E side-E'></div><div class='dir-but dir-but-S side-E'></div>";
    if (dir == "S") return "<div class='dir-but dir-but-E side-S'></div><div class='dir-but dir-but-S side-S'></div>";
    if (dir == "W") return "<div class='dir-but dir-but-S side-W'></div><div class='dir-but dir-but-W side-W'></div>";

    return "?";
}

function createDefaultTarget(c, l) {
    var defTarget = (c + 1) + "-" + l + "-E";
    if (c == (board.c - 1)) {
        defTarget = c + "-" + (l + 1) + "-S";
        if (l == (board.l - 1)) {
            defTarget = (c - 1) + "-" + l + "-W";
        }
    }
    return defTarget;
}
function setCellStyle(c, l, type) {

    var currentType = cellGetStatus(c, l);

    if (currentType != type) {

        switch (type) {
            default:
                break;
            case CS_LETTER:
                delete defCells[cellKey(c, l)];
                sol[c][l] = " ";
                break;
            case CS_BLACK:
                delete defCells[cellKey(c, l)];
                sol[c][l] = "*";
                cells[c][l] = " ";
                break;
            case CS_DEFS: {
                sol[c][l] = "?";
                cells[c][l] = " ";
                var defTarget = createDefaultTarget(c, l);
                defCells[cellKey(c, l)] = { dir: "F", defs: [{ target: defTarget, text: "def" }] };
            }
        }

        redrawBackground();
        redrawInput();
        inputCellInit(c, l);
    }
}


function removeDef(c, l, i) {
    var d = defCells[cellKey(c, l)].defs;
    d.splice(i, 1);
    // remove space informations
    for (var i = 0; i < d.length; i++) {
        delete d[i].space;
    }
    redrawBackground();
    inputCellInit(c, l);
}
function addDefAfter(c, l, i) {

    var d = defCells[cellKey(c, l)].defs;

    var lBefore = d.length;

    var defTarget = createDefaultTarget(c, l);
    d.splice(i, 0, { target: defTarget, text: "def" });
    // remove space informations
    for (var i = 0; i < d.length; i++) {
        delete d[i].space;
    }

    if (lBefore == 1) {
        // was single (F), we have to chose one option, le's say horizontal...
        defCells[cellKey(c, l)].dir = "H";
    }

    redrawBackground();
    inputCellInit(c, l);
}

function moveDef(c, l, i, offset) {
    var d = defCells[cellKey(c, l)].defs;

    var tmp = d.splice(i, 1);
    d.splice(i + offset, 0, tmp[0]);

    redrawBackground();
    inputCellInit(c, l);
}

function inputCellInit(c, l) {
    var s = cellGetStatus(c, l);

    var content = "<div id='cell-type'>Case " + c + "-" + l + "<br>Type case : ";
    $.each([CS_LETTER, CS_BLACK, CS_DEFS], function (index, value) {
        content += "<a href='javascript:setCellStyle(" + c + "," + l + "," + value + ");' class='link-but" + (value == s ? " selected-option" : "") + "'>" + stylesLegends[value] + "</a>";
    });
    content += "</div>";

    switch (s) {
        default:
            content += "Inconnu...";
            break;
        case CS_BLACK:
            content += "Noire";
            break;
        case CS_DEFS:
            content += "Définition(s)";
            if (def = defCells[c + "-" + l]) {

                content += "<div>Division : "

                $.each([CSD_FULL, CSD_VERT, CSD_HORZ], function (index, value) {
                    //alert( index + ": " + value );
                    content += "<a href='javascript:setDefDir(" + c + "," + l + ",\"" + value + "\");' class='link-but" + (value == def.dir ? " selected-option" : "") + "'>" + dirLegends[value] + "</a>";
                });

                for (var i = 0; i < def.defs.length; i++) {

                    var links = "";
                    if (def.defs.length > 1) {
                        if (i > 0) links += " <a class='deb-up' href='javascript:moveDef(" + c + "," + l + "," + i + ",-1)'>" + "Monter" + "</a>"
                        if (i < (def.defs.length - 1)) links += " <a class='deb-down' href='javascript:moveDef(" + c + "," + l + "," + i + ",1)'>" + "Descendre" + "</a>"
                        links += " <a class='deb-remove' href='javascript:removeDef(" + c + "," + l + "," + i + ");'>" + "Supprimer" + "</a>"
                    }
                    content += "<div class='def-block' id='def-" + i + "'>Def " + i + " : " + links;

                    content += "\
                    <table class='def-table' cellspacing='0' cellpadding='0'>\
                    <tr><td></td><td>"+ getDirDivs("N") + "</td><td></td></tr>\
                    <tr><td>"+ getDirDivs("W") + "</td><td class='text-edit'><textarea class='table-input'>" + def.defs[i].text + "</textarea></td><td>" + getDirDivs("E") + "</td></tr>\
                    <tr><td></td><td>"+ getDirDivs("S") + "</td><td></td></tr>\
                    </table>";


                    content += "</div>";
                }

                content += "<div><a class='link-but deb-add' href='javascript:addDefAfter(" + c + "," + l + "," + i + ");'>" + "Ajouter définition" + "</a></div>"

                content += "</div>";
            }
            break;
        case CS_LETTER:
            content += "Lettre : " + cells[c][l];
            break;
    }


    $("#ui-content").html(content);


    if (s == CS_DEFS) {
        // update directions buttons
        for (var i = 0; i < def.defs.length; i++) {
            var d = def.defs[i];
            var m = d.target.match(/^(\d+)-(\d+)-([NEWS])$/);
            var c2 = parseInt(m[1]);
            var l2 = parseInt(m[2]);
            var dir = m[3];
            var border = "E";
            if ((c - c2) < 0) border = "E";
            if ((c - c2) > 0) border = "W";
            if ((l - l2) < 0) border = "S";
            if ((l - l2) > 0) border = "N";
            // current selection
            $("#def-" + i + " .side-" + border + ".dir-but-" + dir).addClass("dir-but-selected");
            // add clicks
            $("#def-" + i + " .dir-but").each(function () {
                var classes = $(this).attr("class");
                if ((m1 = classes.match(/dir-but-([NEWS])/)) && (m2 = classes.match(/side-([NEWS])/))) {
                    var p1 = m1[1];
                    var p2 = m2[1];
                    var p0 = i;
                    $(this).on("click", function () {
                        setDefCellDir(p0, c, l, p1, p2);
                    });
                }
            });

            // remove impossible choices
            if (c == 0) $("#def-" + i + " .side-W").remove();
            if (l == 0) $("#def-" + i + " .side-N").remove();
            if (c == (board.c - 1)) $("#def-" + i + " .side-E").remove();
            if (l == (board.l - 1)) $("#def-" + i + " .side-S").remove();

            // init editable cells
            $("#def-" + i + " .text-edit textarea").on("keydown", function (e) {
                e.stopPropagation();
            });

            (function (i, d) {
                $("#def-" + i + " .text-edit textarea").bind('input propertychange', function () {
                    var v = $(this).val();
                    if (v != d.text) {
                        d.text = v;
                        redrawBackground();
                    }
                });
            })(i, d);
        }
    }
}
function setDefCellDir(defNum, c, l, dir, border) {
    var d = defCells[cellKey(c, l)];
    //console.log("setDefCellDir",defNum, c,l,dir,border);
    var tCell = { c: c, l: l };
    switch (border) {
        default:
            console.log("error: unexpected border value : ", border);
            break;
        case "N":
            tCell.l -= 1;
            break;
        case "S":
            tCell.l += 1;
            break;
        case "W":
            tCell.c -= 1;
            break;
        case "E":
            tCell.c += 1;
            break;
    }
    d.defs[defNum].target = tCell.c + "-" + tCell.l + "-" + dir;
    // refresh
    redrawBackground();
    inputCellInit(c, l);
}

function selectCell(c, l) {
    if ((c >= 0) && (l >= 0) && (c < board.c) && (l < board.l) &&
        (((sol[c][l] != "*") && (sol[c][l] != "?")) || editMode)) {
        /*if ((ui.x == c) && (ui.y == l)) {
            // same cell, change direction
            dirInput[0] = dirInput[0] == 0 ? 1 : 0;
            dirInput[1] = dirInput[1] == 0 ? 1 : 0;
        }*/
        ui.x = c;
        ui.y = l;
    }
    if (editMode) {
        inputCellInit(c, l);
    }
    if (mobileMode) {
        Keyboard.open("", currentValue => {
            console.log(currentValue);
        },
            null,
            currentKey => {
                console.log(currentKey);
                var e = new KeyboardEvent("keydown", { key: currentKey });
                processKey(e);
            });
    }
    redrawInput();
}

function editableCell(x, y) {

    if ((x >= 0) && (y >= 0) && (x < board.c) && (y < board.l) &&
        (sol[x][y] != "*") && (sol[x][y] != "?"))
        return true;
    return false;
}
function validCell(x, y) {
    if ((x >= 0) && (y >= 0) && (x < board.c) && (y < board.l))
        return true;
    return false;
}

Number.prototype.mod = function (n) {
    return ((this % n) + n) % n;
};
function shiftCells(offset) {
    var tmpCells = [];
    var tmpSol = [];
    // create empty matrix
    for (var x = 0; x < board.c; x++) {
        tmpSol[x] = [];
        tmpCells[x] = [];
        for (var y = 0; y < board.l; y++) {
            tmpSol[x][y] = " ";
            tmpCells[x][y] = " ";
        }
    }
    // paste shifted content
    for (var x = 0; x < board.c; x++) {
        for (var y = 0; y < board.l; y++) {
            tmpSol[(x + offset[0]).mod(board.c)][(y + offset[1]).mod(board.l)] = sol[x][y];
            tmpCells[(x + offset[0]).mod(board.c)][(y + offset[1]).mod(board.l)] = cells[x][y];
        }
    }
    // recopy shifted content
    for (var x = 0; x < board.c; x++) {
        for (var y = 0; y < board.l; y++) {
            sol[x][y] = tmpSol[x][y];
            cells[x][y] = tmpCells[x][y];
        }
    }

    // update defs
    var newDefs = {};
    for (var d in defCells) {
        var def = defCells[d];
        var m = d.match(/^(\d+)-(\d+)$/);
        var c = parseInt(m[1]);
        var l = parseInt(m[2]);
        var newKey = cellKey((c + offset[0]).mod(board.c), (l + offset[1]).mod(board.l));
        newDefs[newKey] = { dir: def.dir, defs: [] };
        for (var i = 0; i < def.defs.length; i++) {
            newDefs[newKey].defs[i] = { text: def.defs[i].text };
            if (typeof def.defs[i].space !== 'undefined') newDefs[newKey].defs[i].space = def.defs[i].space;

            var m2 = def.defs[i].target.match(/^(\d+)-(\d+)-([NEWS])$/);
            var c2 = parseInt(m2[1]);
            var l2 = parseInt(m2[2]);
            var dir2 = m2[3];
            newDefs[newKey].defs[i].target = (c2 + offset[0]).mod(board.c) + "-" + (l2 + offset[1]).mod(board.l) + "-" + dir2;
        }
    }
    // recopy shifted content
    defCells = JSON.parse(JSON.stringify(newDefs));
}

function checkScore() {
    var nbLetters = 0;
    var nbOK = 0;
    for (var c = 0; c < board.c; c++) {
        for (var l = 0; l < board.l; l++) {
            if (editableCell(c, l)) {
                nbLetters++;
                if (cells[c][l] == sol[c][l]) {
                    nbOK++;
                }
            }
        }
    }
    //console.log("Score = "+Math.floor(nbOK/nbLetters*100)+" ("+nbOK+"/"+nbLetters+")");
    return Math.floor(nbOK / nbLetters * 100); // in prct
}

/*var mobKB = null ;*/
function resetBoard(bResetContent = true) {

    if (bResetContent) {

        // prepare cells
        for (var c = 0; c < board.c; c++) {
            cells[c] = [];
            for (var l = 0; l < board.l; l++) {
                if (editMode) {
                    cells[c][l] = sol[c][l];
                }
                else {
                    cells[c][l] = " ";
                }
            }
        }
    }

    board.W = board.c * cfg.w;
    board.H = board.l * cfg.h;

    var cnv = $("#board-bg")[0];
    cnv.width = board.W;
    cnv.height = board.H;

    $("#board-container").css("width", board.W + "px");
    $("#board-container").css("margin", "0 auto");

    var cnvInput = $("#board-input")[0];
    cnvInput.width = board.W;
    cnvInput.height = board.H;

    redrawBackground();
    redrawInput();

    /*    if (!mobKB){
            mobKB = new Keyboard(keybConfFr);
        }*/
}

var poweredHtml = "(propulsé par <a target='_blank' href='https://framagit.org/jcfrog/jcxword'>jcxword</a>.)";
function loadBoard(id) {
    if (ed = urlParams.get("edit")) {
        if (ed == "y") editMode = true;
    }
    $.getScript("boards/" + id + ".js").done(function (script, textStatus) {
        console.log("Load " + id + " was performed.");
        if (textStatus == "success") {

            if (gridName !== undefined) {
                $("#grid-name").text(gridName);
            }
            if (authorHtml !== undefined) {
                $("#author").html(authorHtml + " • " + poweredHtml);
            } else {
                $("#author").html(poweredHtml)
            }

            sol = [];
            for (var c = 0; c < solInit[0].length; c++) {
                sol[c] = [];
                for (var l = 0; l < solInit.length; l++) {
                    sol[c][l] = solInit[l][c];
                }
            }

            board = {
                c: sol.length,
                l: sol[0].length,
                W: sol.length * cfg.w,
                H: sol[0].length * cfg.h,
            }
        }
        resetBoard();
        loadStringifiedBoard(localStorage.getItem("board-"+id));
    }).fail(function (jqxhr, settings, exception) {
        if ((newc = urlParams.get("c")) && (newr = urlParams.get("r"))) {
            var c = parseInt(newc);
            var r = parseInt(newr);
            board = {
                c: c,
                l: r,
                W: c * cfg.w,
                H: r * cfg.h,
            }
            sol = [];
            for (var i = 0; i < c; i++) {
                sol[i] = [];
                for (var j = 0; j < r; j++) {
                    sol[i][j] = " ";
                }
            }
            defCells = {};

            resetBoard();
        } else {
            $("header").empty();
            $("header").append("<div id=\"create-header\"><p>" +
                t("Create grid") + " (" + t("columns x rows") + ")" +
                " : <input type=\"number\" size=\"2\" id=\"nb-columns\" name=\"nb-columns\" min=\"2\" max=\"100\" value=10> x \
                <input type=\"number\" size=\"2\" id=\"nb-rows\" name=\"nb-rows\" min=\"2\" max=\"100\" value=8>"+
                "<a class=\"create-but\" href=\"javascript:createGrid();\">" + t("Start") + "</a>"
                + "</p>"
                + "</div>");
        }
    });
}

function createGrid() {
    var c = $("#nb-columns").val();
    var r = $("#nb-rows").val();
    window.open("?edit=y&c=" + c + "&r=" + r);
}

function processKey(e) {
    var key = e.key;

    // history
    if (((key == "Z") || (key == "z")) && e.ctrlKey) {
        undo();
        return;
    }
    if (((key == "Y") || (key == "y")) && e.ctrlKey) {
        redo();
        return;
    }


    var letterNumber = /^[a-zA-Z]$/;
    if (key.match(letterNumber)) {
        if (editableCell(ui.x, ui.y)) {
            addStepToHistory();
            cells[ui.x][ui.y] = key.toUpperCase();
            if (id = urlParams.get("id")){
                localStorage.setItem("board-"+id,stringifyBoard());
            }        
            selectCell(
                Math.max(0, Math.min(ui.x + dirInput[0], board.c - 1)),
                Math.max(0, Math.min(ui.y + dirInput[1], board.l - 1))
            );
            lastInput = [ui.x, ui.y];
        }
    }

    if (editMode && ((key == "?") || (key == "*"))) {
        if (validCell(ui.x, ui.y)) {
            if (key == "?") setCellStyle(ui.x, ui.y, CS_DEFS);
            if (key == "*") setCellStyle(ui.x, ui.y, CS_BLACK);
            selectCell(
                Math.max(0, Math.min(ui.x + dirInput[0], board.c - 1)),
                Math.max(0, Math.min(ui.y + dirInput[1], board.l - 1))
            );
            lastInput = [ui.x, ui.y];
        }
    }

    var bUpdate = false;
    var bUpdateBg = false;
    switch (e.key) {
        default:
            break;
        case "ArrowRight":
            if (editMode && e.ctrlKey) {
                shiftCells([1, 0]);
                bUpdateBg = true;
            } else {
                ui.x = Math.max(0, Math.min(ui.x + 1, board.c - 1));
            }
            bUpdate = true;
            break;
        case "ArrowLeft":
            if (editMode && e.ctrlKey) {
                shiftCells([-1, 0]);
                bUpdateBg = true;
            } else {
                ui.x = Math.max(0, Math.min(ui.x - 1, board.c - 1));
            }
            bUpdate = true;
            break;
        case "ArrowDown":
            if (editMode && e.ctrlKey) {
                shiftCells([0, 1]);
                bUpdateBg = true;
            } else {
                ui.y = Math.max(0, Math.min(ui.y + 1, board.l - 1));
            }
            bUpdate = true;
            break;
        case "ArrowUp":
            if (editMode && e.ctrlKey) {
                shiftCells([0, -1]);
                bUpdateBg = true;
            } else {
                ui.y = Math.max(0, Math.min(ui.y - 1, board.l - 1));
            }
            bUpdate = true;
            break;
        case "PageDown":
            dirInput = [0, 1];
            bUpdate = true;
            e.preventDefault();
            break;
        case "End":
            dirInput = [1, 0];
            bUpdate = true;
            e.preventDefault();
            break;
        case " ":
            dirInput[0] = dirInput[0] == 0 ? 1 : 0;
            dirInput[1] = dirInput[1] == 0 ? 1 : 0;
            bUpdate = true;
            e.preventDefault();
            break;
    }
    if (e.keyCode == 46) { // suppr
        if (editableCell(ui.x, ui.y)) {
            cells[ui.x][ui.y] = " ";
            bUpdate = true;
        }
    }
    if (e.keyCode == 8) { // backspace
        if (editableCell(ui.x - dirInput[0], ui.y - dirInput[1])) {
            cells[ui.x][ui.y] = " ";
            ui.x -= dirInput[0];
            ui.y -= dirInput[1];
            cells[ui.x][ui.y] = " ";
            bUpdate = true;
        }
    }

    if (bUpdateBg) {
        redrawBackground();
    }

    if (bUpdate) {
        //e.preventDefault();
        redrawInput();
        if (editMode) {
            inputCellInit(ui.x, ui.y);
        }
    }
}

$(document).ready(function () {
    // create html elements just once
    $("#board-container").append("<canvas id='board-bg' class='board-layer'></canvas>");
    $("#board-container").append("<canvas id='board-input' class='board-layer'></canvas>");


    // events
    $("#board-input").click(function (e) {
        selectCell(Math.floor(e.offsetX / cfg.w), Math.floor(e.offsetY / cfg.h));
        //prompt();
    });

    $(document).keydown(function (e) {

        processKey(e);

    });

    // load board data
    if (id = urlParams.get("id")) {
        loadBoard(id);
    } else {
        loadBoard("");
        console.log("Warning : no board id to be loaded...");
    }



    if (mobileMode) {
        //$("header").prepend("<div id='mobile-banner'>yo</div>");
        Keyboard.init();
    }

    // UI box
    if (editMode) {
        $("body").append("\
            <div id='uibox'>\
                <div id='ui-handle'></div>\
                <div id='ui-content'></div>\
            </div>");
        $("#uibox").draggable();
        $("#ui-handle").on("click", function (e) {
            $("#uibox").toggleClass("ui-folded");
        });
        $("#uibox").css("left", "200px");

        $("#help").append("<div><a href='javascript:dumpSolution();'>Générer le code</a>\
         • <a href='javascript:addCol();'>Ajouter colonne</a>\
         • <a href='javascript:addLine();'>Ajouter ligne</a>\
         • (déplacer contenu : Ctrl + flèches)\
         </div>");

        $("body").append("<div id='dumper'><pre>Hello</pre></div>");

    }

    if (!editMode){
        // commands
        var offerLetter = $("<a/>").attr("href","#").text("Révéler une lettre").click(giveALetter);
        $("#commands").append(offerLetter);
        var offerSolution = $("<a/>").attr("href","#").text("Révéler la solution").click(giveSolution);
        $("#commands").append(" • ");
        $("#commands").append(offerSolution);
        var reset = $("<a/>").attr("href","#").text("Vider la grille").click(restart);
        $("#commands").append(" • ");
        $("#commands").append(reset);
        $("#commands").append(" • Score : <span id='score'></span>");
    }   
});


// edit mode functions
function addCol() {
    sol[board.c] = [];
    cells[board.c] = [];
    for (var l = 0; l < board.l; l++) {
        sol[board.c][l] = " ";
        cells[board.c][l] = " ";
    }
    board.c++;
    resetBoard(false);
}
function addLine() {
    for (var c = 0; c < board.c; c++) {
        sol[c][board.l] = " ";
        cells[c][board.l] = " ";
    }
    board.l++;
    resetBoard(false);
}

function dumpSolution() {
    var dmp = "";
    var CRLF = "\n";
    //dmp += ""+CRLF;
    dmp += "var gridName = \"" + (typeof gridName !== 'undefined' ? gridName : "grid name") + "\";" + CRLF;
    dmp += "var authorHtml = \"" + (typeof authorHtml !== 'undefined' ? authorHtml : "Grille de <a href='#'>Author</a>") + "\";" + CRLF;
    dmp += CRLF;
    dmp += "var solInit = [" + CRLF;
    for (var l = 0; l < board.l; l++) {
        var str = "";
        for (var c = 0; c < board.c; c++) {
            str += (sol[c][l] == "?" || sol[c][l] == "*") ? sol[c][l] : cells[c][l];
        }
        dmp += "\"" + str + "\"," + CRLF;
    }
    dmp += "];" + CRLF;

    dmp += CRLF;

    dmp += "defCells = {" + CRLF;
    for (d in defCells) {
        var def = defCells[d];
        dmp += "\"" + d + "\" : ";
        dmp += "{ ";
        dmp += "dir : \"" + def.dir + "\" , defs : [";
        for (var i = 0; i < def.defs.length; i++) {
            if (i > 0)
                dmp += " , ";
            dmp += "{ target : \"" + def.defs[i].target + "\", text : \"" + def.defs[i].text + "\"";
            if (typeof def.defs[i].space !== 'undefined')
                dmp += ", space : " + def.defs[i].space;
            dmp += " }";
        }
        dmp += " ] }," + CRLF;

    }
    dmp += "}" + CRLF;


    $("#dumper pre").text(dmp);
}


function restart(){
    addStepToHistory();
    if (id = urlParams.get("id")){
        localStorage.setItem("board-"+id,"");
    }
    resetBoard();
}   

function giveSolution(){
    if(confirm("Afficher toute la solution?")){
        for (var c = 0; c < board.c; c++) {
            for (var l = 0; l < board.l; l++) {
                if (editableCell(c, l)) {
                    cells[c][l] = sol[c][l];
                }
            }
        }
        redrawBackground();
        redrawInput();
    }
}

function giveALetter(){
    var nbLetters = 0;    
    var nbOK = 0;
    var letters=[];
    for (var c = 0; c < board.c; c++) {
        for (var l = 0; l < board.l; l++) {
            if (editableCell(c, l)) {
                nbLetters++;
                if (cells[c][l] == sol[c][l]) {
                    nbOK++;
                }else{
                    letters.push({c:c,l,l});
                }
            }
        }
    }
    if(letters.length > 0){
        addStepToHistory();
        var n = Math.floor(Math.random()*letters.length);
        var c = letters[n].c;
        var l = letters[n].l;
        cells[c][l] = sol[c][l] ;
        ui.x=c;
        ui.y=l;
        redrawBackground();
        redrawInput();
    }    
}


// keyboard emulation 
/*const keybConfFr = [    
    "AZERTYUIOP",
    "QSDFGHJKLM",
    "WXCVBN<>^v"
    ];
class mobKey{
    constructor(letter){
        this.letter = letter ;                
    }
    label(){
        return this.letter ;
    }
    getCode(){
        return("<div class='mob-key noselect'>"+this.label()+"</div>");
    }
}
class Keyboard{
    constructor(kbConf){
        var c = $("<div/>").addClass('keyb-container').attr("id","mobile-keyb");
        for (var i = 0 ; i < kbConf.length ; i++){
            for (var l = 0 ; l < kbConf[i].length ; l++){
                var k = new mobKey(kbConf[i][l]);
                c.append(k.getCode());
            }
        }
        //$("#"+parentId).append(c);
        $("body").append(c);
    }
}*/

// translations
var translations = {
    "Play Game": { fr: "Jouer seul" },
}
function t(txt) {
    if (translations[txt] && translations[txt][lg]) {
        return translations[txt][lg];
    }
    return txt;
}