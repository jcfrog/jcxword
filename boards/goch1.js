var gridName = "Grille de gauchiste";
var authorHtml = "Grille de <a href='https://jcfrog.com'>jcfrog</a>";



var solInit = [
    "?EXPLOITATION",
    "?PEAUX???ANTI",
    "B?RU?YMCA?DEA",
    "A?OLIGARQUE?I",
    "BOXE?ERO?RCA?",
    "OU??ANXIOGENE",
    "RI?PRE?SIENS*",
    "DROIT?REA?TIG",
];

defCells = {
    "2-5": { dir: "H", defs: [{ target: "2-4-N", text: "Consommateur de consommables" }, { target: "1-5-W", text: "Conjonction" }] },
    "0-0": { dir: "H", defs: [{ target: "1-0-E", text: "Emploi" }, { target: "1-0-S", text: "Disque" }] },
    "3-5": { dir: "H", defs: [{ target: "3-4-N", text: "Elle aurait pu être employeur" }, { target: "4-5-E", text: "Stressant" }, { target: "3-6-S", text: "Constante" }] },
    "6-1": { dir: "F", defs: [{ target: "6-2-S", text: "Référence barbue" }] },
    "6-6": { dir: "F", defs: [{ target: "7-6-E", text: "Font partie de sa propriété privée" }] },
    "7-1": { dir: "F", defs: [{ target: "7-2-S", text: "Métisse" }] },
    "5-7": { dir: "H", defs: [{ target: "4-7-W", text: "Comme un i" }, { target: "6-7-E", text: "Quand on manque d'air" }] },
    "2-6": { dir: "H", defs: [{ target: "1-6-W", text: "S'est amusé" }, { target: "3-6-E", text: "Signifie son avance" }] },
    "11-3": { dir: "H", defs: [{ target: "11-2-N", text: "Enlève" }, { target: "11-4-S", text: "Chef des normes" }] },
    "12-4": { dir: "F", defs: [{ target: "12-3-N", text: "Je dis non" }] },
    "0-1": { dir: "H", defs: [{ target: "1-1-E", text: "Organes de surface" }, { target: "0-2-S", text: "Une autre vision de la gauche" }] },
    "8-1": { dir: "H", defs: [{ target: "9-1-E", text: "Contre" }, { target: "8-2-S", text: "Domaine polaire" }] },
    "9-2": { dir: "H", defs: [{ target: "9-1-N", text: "Téraampère" }, { target: "10-2-S", text: "Choque la morale" }, { target: "10-2-E", text: "Permettait d'approfondir" }] },
    "1-2": { dir: "F", defs: [{ target: "2-2-E", text: "On y roule du bon côté" }] },
    "1-3": { dir: "H", defs: [{ target: "2-3-E", text: "Maître de l'Est" }, { target: "1-4-S", text: "Nécessite de tendre l'oreille" }] },
    "4-2": { dir: "H", defs: [{ target: "4-1-N", text: "Petit cookie" }, { target: "5-2-S", text: "Dans l'air et dans l'eau" }, { target: "5-2-E", text: "Chanson de villageois" }] },
    "4-4": { dir: "H", defs: [{ target: "3-4-W", text: "Mieux avec des gants" }, { target: "5-4-E", text: "Genre d'araignée" }, { target: "4-5-S", text: "Activité pour les sens" }] },
    "8-4": { dir: "H", defs: [{ target: "9-4-E", text: "Connectique" }, { target: "8-5-S", text: "Araignée d'Asie" }] },
    "9-7": { dir: "H", defs: [{ target: "9-6-N", text: "Presse" }, { target: "10-7-E", text: "Punition légère" }] },
}
