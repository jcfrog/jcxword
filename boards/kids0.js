var gridName = "Animaux";
var authorHtml = "Grille de <a href='https://jcfrog.com'>Jérôme</a>";

var solInit = [
"**?*?F",
"?POULE",
"**I*OR",
"ETE?U*",
"****P*",
];

defCells = {
"0-1" : { dir : "F" , defs : [{ target : "1-1-E", text : "Fait de bons oeufs" } ] },
"3-3" : { dir : "F" , defs : [{ target : "2-3-W", text : "Saison du soleil" } ] },
"2-0" : { dir : "F" , defs : [{ target : "2-1-S", text : "Volaille" } ] },
"4-0" : { dir : "H" , defs : [{ target : "5-0-S", text : "Métal" } , { target : "4-1-S", text : "Pas toujours grand et méchant" } ] },
}

var xtraInfos = {
    gridType : "intersect" // "intersect", "cross","arrow"
}