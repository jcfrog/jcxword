var gridName = "Les mots qui font Grrille";
var authorHtml = "Grille de <a href='https://linktr.ee/klaire'>Klaire</a>";

var solInit = [
    "*????*??",
    "?AMIANTE",
    "?NOEL?ET",
    "?ENLAIDI",
    "EST?RIXE",
    "?*E?MI?N",
    "TABLE?ON",
    "A*O??LUE",
    "R?UFC*R?",
    "T?ROUSSE",
    "E?GUI*EX",
    ];

defCells = {
"1-0" : { dir : "F" , defs : [{ target : "1-1-S", text : "Bêtes qui crèchent là" } ] },
"2-0" : { dir : "F" , defs : [{ target : "2-1-S", text : "Télé-conseillé le plus zélé de France" } ] },
"3-0" : { dir : "F" , defs : [{ target : "3-1-S", text : "Péril mortel de Jean-Michel" } ] },
"4-0" : { dir : "F" , defs : [{ target : "4-1-S", text : "Doit avoir la sonnette usée à force" } ] },
"6-0" : { dir : "F" , defs : [{ target : "6-1-S", text : "Conférence sans pupitre" } ] },
"7-0" : { dir : "F" , defs : [{ target : "7-1-S", text : "Devrait bien se tenir" } ] },
"0-1" : { dir : "F" , defs : [{ target : "1-1-E", text : "Matière à scandale" } ] },
"0-2" : { dir : "F" , defs : [{ target : "1-2-E", text : "Après l'avent" } ] },
"5-2" : { dir : "H" , defs : [{ target : "6-2-E", text : "Alien quasi-quadra" } , { target : "5-3-S", text : "Trois à Rome" } ] },
"0-3" : { dir : "H" , defs : [{ target : "1-3-E", text : "Amoché" } , { target : "0-4-E", text : "Grand pour la région" } ] },
"3-4" : { dir : "F" , defs : [{ target : "4-4-E", text : "Pugilat" } ] },
"3-5" : { dir : "F" , defs : [{ target : "4-5-E", text : "Plutôt bas de gamme" } ] },
"6-5" : { dir : "F" , defs : [{ target : "6-6-S", text : "En grand dans le ciel" } ] },
"5-6" : { dir : "F" , defs : [{ target : "6-6-E", text : "Les gens" } ] },
"4-7" : { dir : "H" , defs : [{ target : "5-7-E", text : "Prend une autre voix" } , { target : "4-8-S", text : "Demi cri d'oiseau" } ] },
"3-7" : { dir : "F" , defs : [{ target : "3-8-S", text : "Traîne avec les pions" } ] },
"0-5" : { dir : "V" , defs : [{ target : "0-6-E", text : "Bien dressée pour les fêtes" } , { target : "0-6-S", text : "Sa pelle est un sacré col" } ] },
"7-8" : { dir : "F" , defs : [{ target : "7-9-S", text : "Quelque chose d'ancien" } ] },
"1-8" : { dir : "F" , defs : [{ target : "2-8-E", text : "Ne sait jamais que choisir" } ] },
"1-9" : { dir : "F" , defs : [{ target : "2-9-E", text : "Poile de carotte" } ] },
"1-10" : { dir : "F" , defs : [{ target : "2-10-E", text : "Excuse de merde pour bise forcée" } ] },
}