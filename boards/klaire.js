var gridName = "Les mots qui font Grrille";
var authorHtml = "Grille de <a href='https://linktr.ee/klaire'>Klaire</a>";

var solInit = [
    "*??E??IL",
    "?DONALD*",
    "?EUROPE?",
    "?MIAM?AN",
    "SI?G?PLO",
    "?SOEUR?N",
    "DSL?SIR?",
    "III?AE?E",
    "?OVAIRES",
    "?NE?TEST"
];

// Keys format : "c-l" : c = column , l = line
// dir : [F = full , H = horizontal split , V = vertical split ] 
// target format (coordinates of word's adjacent cell + direction) "c-l-d" : c = column , l = line , d = direction
// N / E / W / S = North, Est, West, South for target word direction
// you can add field "space" to give percentage of each splitted area of the definition cell
// example with horizontal split with top cell 30% of height: 
//      "0-0": { dir : "H", defs : [{ target: "1-0-S", text: "text 1 " , space : 30 }, { target: "0-1-S", text: "longer text 2", space : 70 }] }
var defCells = {
    "1-0": {dir : "F", defs : [{ target: "1-1-S", text: "On attend celle à Gégé" }]},
    "2-0": {dir : "H", defs : [{ target: "3-0-S", text: "S'énèrve avant Pasteur" }, { target: "2-1-S", text: "Entendu il y a longtemps" }]},
    "4-0": {dir : "F", defs : [{ target: "4-1-S", text: "Célèbre col en désordre" }]},
    "0-2": {dir : "F", defs : [{ target: "1-2-E", text: "La numéro 1 passe à l'ennemi" }]},
    "5-0": {dir : "V", defs : [{ target: "5-1-S", text: "Abrégé de courses peu chères" }, { target: "6-0-E", text: "Mieux payé qu'elle" }]},
    "0-1": {dir : "F", defs : [{ target: "1-1-E", text: "Canard juste bon pour les WC" }]},
    "7-2": {dir : "F", defs : [{ target: "7-3-S", text: "\"Non\" dans le langage mystérieux des femmes" }]},
    "0-3": {dir : "H", defs : [{ target: "1-3-E", text: "Cri du coeur sur la table" }, { target: "0-4-E", text: "Donne super mal le la" }]},
    "5-3": {dir : "H", defs : [{ target: "6-3-E", text: "On en fête chaque nouveau" }, { target: "5-4-S", text: "Requête au père" }]},
    "2-4": {dir : "F", defs : [{ target: "2-5-S", text: "Vit en bocal ou en Méditerranée" }]},
    "4-4": {dir : "H", defs : [{ target: "5-4-E", text: "Morceau de plomb" }, { target: "4-5-S", text: "Sur-utilisait" }]},
    "0-5": {dir : "H", defs : [{ target: "1-5-E", text: "Exclue de la fraternité" }, { target: "0-6-E", text: "Excuse du bout des doigts" }]},
    "6-5": {dir : "F", defs : [{ target: "6-4-N", text: "Modèle de perfection" }]},
    "3-6": {dir : "F", defs : [{ target: "4-6-E", text: "En première partie d'Elton John" }]},
    "7-6": {dir : "F", defs : [{ target: "7-7-S", text: "Carrément pas à l'Ouest" }]},
    "3-7": {dir : "H", defs : [{ target: "4-7-E", text: "Initiales relativement connues" }, { target: "2-7-W", text: "Trois romain" }]},
    "6-7": {dir : "F", defs : [{ target: "6-8-S", text: "Vis" }]},
    "0-8": {dir : "H", defs : [{ target: "0-7-N", text: "Lady" }, { target: "1-8-E", text: "Boite à oeufs" }]},
    "0-9": {dir : "F", defs : [{ target: "1-9-E", text: "Vu le jour" }]},
    "3-9": {dir : "F", defs : [{ target: "4-9-E", text: "Se fait avec ou sans pipi" }]}
}
