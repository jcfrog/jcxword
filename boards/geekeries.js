var gridName = "Geekeries";
var authorHtml = "Grille de <a href='https://jcfrog.com'>jcfrog</a>";


var solInit = [
    "?NUMERIQUE",
    "VAR?NES?SI ",
    "IRL??BOOB?",
    "RU??NO?R?L",
    "UT?SEO?EPO",
    "SONS?TROLL"
];

// Keys format : "c-l" : c = column , l = line
// dir : [F = full , H = horizontal split , V = vertical split ] 
// N / E / W / S = North, Est, West, South for target word direction
// you can add field "space" to give percentage of each splitted area of the definition cell
// example with horizontal split with top cell 30% of height: 
//      "0-0": { dir : "H", defs : [{ target: "1-0-S", text: "text 1 " , space : 30 }, { target: "0-1-S", text: "longer text 2", space : 70 }] }
defCells = {
    "0-0" : { dir : "H", defs : [{target : "1-0-E", text : "Avant lui il n'y avait rien", space : 40},{target : "1-0-S", text : "Manga", space : 20},{target : "0-1-S", text : "Les antis le sont parfois", space : 40}]},
    "3-3" : { dir : "H", defs : [{target : "4-3-E", text : "Numéro"},{target : "3-4-S", text : "Pas super sympas"}]},
    "4-2" : { dir : "H", defs : [{target : "4-1-N", text : "Fournisseur historique de tutoriels", space: 50},{target : "5-2-E", text : "Pis des internets", space: 25},{target : "4-3-S", text : "Toujours négatif", space: 25}]},
    "3-2" : { dir : "F", defs : [{target : "2-2-W", text : "On s'y ennuyait"}]},
    "3-1" : { dir : "H", defs : [{target : "2-1-W", text : "Manque de constance"},{target : "4-1-E", text : "Console"}]},
    "2-4" : { dir : "H", defs : [{target : "1-4-W", text : "Clé publique"},{target : "3-4-E", text : "La voie du succès"}]},
    "2-3" : { dir : "H", defs : [{target : "2-2-N", text : "Un lieu unique"}, {target : "1-3-W", text : "Cours d'eau"}]},
    "6-3" : { dir : "H", defs : [{target : "6-2-N", text : "Grosse image"},{target : "5-3-S", text : "Peut faire du bien"}]},
    "4-5" : { dir : "H", defs : [{target : "3-5-W", text : "MP3s"},{target : "5-5-E", text : "Pas super sympa"}]},
    "7-1" : { dir : "H", defs : [{target : "8-1-E", text : "Fait l'unité"},{target : "7-2-S", text : "Cookie acceptable"}]},
    "6-4" : { dir : "F", defs : [{target : "7-4-E", text : "En synthétique c'est moins bien"}]},
    "9-2" : { dir : "H", defs : [{target : "9-1-N", text : "Pas hyper sympa"},{target : "9-3-S", text : "Comme on s'amuse"}]},
    "8-3" : { dir : "H", defs : [{target : "8-2-N", text : "Et tout devint facile"},{target : "8-4-S", text : "Bouge malgré son poids"}]},
}