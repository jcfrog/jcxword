var gridName = "Grille de droite";
var authorHtml = "Grille de <a href='https://jcfrog.com'>jcfrog</a>";



var solInit = [
    "?DROITE?D?AOC",
    "FIAC?UV?ECHOS",
    "ASSISTES?H?GA",
    "?RO?A?IA?ERE?",
    "RUISSELLEMENT",
    "APRE?T?EPICES",
    "ST?UV?DRONES?",
    "?ISLAMO??OLE?",
    "AF???AMONT?SI",
];

defCells = {
    "0-0": { dir: "H", defs: [{ target: "1-0-E", text: "Ni droite ni gauche", space: 40 }, { target: "1-0-S", text: "Vrai progressiste", space: 40 }, { target: "0-1-S", text: "Note", space: 20 }] },
    "7-0": { dir: "H", defs: [{ target: "8-0-S", text: "Signe de noblesse" }, { target: "6-0-S", text: "Sortie du  sommeil" }] },
    "9-0": { dir: "H", defs: [{ target: "10-0-E", text: "Garantie de bonne lignée" }, { target: "9-1-S", text: "Chevalier de l'apocalypse" }] },
    "5-3": { dir: "H", defs: [{ target: "5-2-N", text: "Passa sous silence" }, { target: "5-4-S", text: "Conjonction" }] },
    "0-7": { dir: "H", defs: [{ target: "0-6-N", text: "Autant se taire" }, { target: "1-7-E", text: "Préfix pour opposants" }] },
    "7-7": { dir: "F", defs: [{ target: "7-6-N", text: "Assaisonner" }] },
    "8-2": { dir: "F", defs: [{ target: "7-2-W", text: "Assurés sociaux" }] },
    "0-3": { dir: "H", defs: [{ target: "1-3-E", text: "Langue construite" }, { target: "0-4-E", text: "Effet de notre bonté" }] },
    "4-1": { dir: "H", defs: [{ target: "3-1-W", text: "Temple de la création spéculative", space: 60 }, { target: "5-1-E", text: "Du mauvais côté du spectre", space: 40 }] },
    "3-3": { dir: "H", defs: [{ target: "3-2-N", text: "Nid de trotskystes" }, { target: "4-3-S", text: "Type d'entreprise" }] },
    "2-6": { dir: "H", defs: [{ target: "2-5-N", text: "Anti poils" }, { target: "1-6-W", text: "Petit Saint" }, { target: "3-6-E", text: "Fonce la peau" }] },
    "5-6": { dir: "H", defs: [{ target: "5-7-S", text: "Possessif" }] },
    "4-5": { dir: "H", defs: [{ target: "3-5-W", text: "Pénible, désagréable" }, { target: "4-6-S", text: "Journal exigeant" }] },
    "10-8": { dir: "H", defs: [{ target: "10-7-N", text: "Commerce délictueux" }, { target: "11-8-E", text: "Do bémol" }] },
    "12-7": { dir: "F", defs: [{ target: "11-7-W", text: "Cri espagnol" }] },
    "7-1": { dir: "H", defs: [{ target: "8-1-E", text: "Journal de l'argent" }] },
    "10-2": { dir: "H", defs: [{ target: "10-1-N", text: "Interjection" }, { target: "11-2-E", text: "Métal fondant" }, { target: "11-2-S", text: "Process d'oeufs" }] },
    "6-5": { dir: "H", defs: [{ target: "7-5-E", text: "Sources de saveurs" }, { target: "6-6-S", text: "Proche de TOM" }] },
    "8-7": { dir: "F", defs: [{ target: "8-6-N", text: "Aide à la performance" }] },
    "8-3": { dir: "H", defs: [{ target: "7-3-W", text: "Pensée surnaturelle" }, { target: "9-3-E", text: "Epoque" }] },
    "12-6": { dir: "F", defs: [{ target: "11-6-W", text: "Nouveaux auxiliaires de police" }] },
    "12-3": { dir: "H", defs: [{ target: "12-2-N", text: "Autorité des vieux médias" }, { target: "12-4-S", text: "Fin ratée" }] },
    "3-8": { dir: "F", defs: [{ target: "3-7-N", text: "Contre tous" }] },
    "4-8": { dir: "F", defs: [{ target: "5-8-E", text: "Plus haut" }] },
    "2-8": { dir: "F", defs: [{ target: "1-8-W", text: "Compagnie aérienne" }] },
}
