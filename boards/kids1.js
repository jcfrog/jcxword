var gridName = "Grille enfants 1";
var authorHtml = "Grille de <a href='https://jcfrog.com'>jerome</a>";

var solInit = [
"?CUBE",
"NO?O?",
"?TUBE",
"ZEN?T",
];

defCells = {
"0-0" : { dir : "H" , defs : [{ target : "1-0-E", text : "Forme du dé" } , { target : "0-1-E", text : "Non anglais" } ] },
"3-3" : { dir : "H" , defs : [{ target : "3-2-N", text : "Chapeau" } , { target : "2-3-W", text : "Détendu" } ] },
"0-2" : { dir : "H" , defs : [{ target : "1-2-S", text : "Protège les poumons" } , { target : "1-2-E", text : "Musique à succès" } ] },
"2-1" : { dir : "F" , defs : [{ target : "2-2-S", text : "Pas deux" } ] },
"4-1" : { dir : "F" , defs : [{ target : "4-2-S", text : "Extra terrestre" } ] },
}