var gridName = "🤘 Métal 🤘";
var authorHtml = "Grille de <a href='https://jcfrog.com'>jcfrog</a>";



var solInit = [
    "?METALLICA",
    "NE???U?PIN",
    "?GOJIRA??T",
    "SABBATH?VH",
    "?DIO?HAVER",
    "DES?C??ENA",
    "ATP?AG?VOX",
    "WHO?SODOM?",
    ];
    
    defCells = {
    "3-1" : { dir : "F" , defs : [{ target : "3-2-S", text : "Fun metal" } ] },
    "0-2" : { dir : "H" , defs : [{ target : "0-1-E", text : "Adverbe" } , { target : "1-2-E", text : "Fierté landaise" } ] },
    "4-4" : { dir : "H" , defs : [{ target : "5-4-E", text : "Action de mineur" } , { target : "4-5-S", text : "Pas toujours raté" } ] },
    "2-1" : { dir : "F" , defs : [{ target : "2-2-S", text : "Fan de Rammstein" } ] },
    "4-1" : { dir : "F" , defs : [{ target : "4-2-S", text : "Plus qu'un algorithme" } ] },
    "6-1" : { dir : "H" , defs : [{ target : "7-1-E", text : "Bois" } , { target : "6-2-S", text : "Nordique mais pas ultra métal" } ] },
    "9-7" : { dir : "H" , defs : [{ target : "9-6-N", text : "Thrash de New York" } , { target : "8-7-W", text : "Thrash germanique" } ] },
    "7-2" : { dir : "F" , defs : [{ target : "7-1-N", text : "Adresse" } ] },
    "7-3" : { dir : "H" , defs : [{ target : "6-3-W", text : "Noir en ce qui nous concerne" } , { target : "8-3-E", text : "Guitariste et batteur" } ] },
    "6-6" : { dir : "H" , defs : [{ target : "5-6-W", text : "Réunit les adhérents" } , { target : "7-6-E", text : "Ampli" } ] },
    "3-7" : { dir : "F" , defs : [{ target : "2-7-W", text : "Pionniers du heavy metal selon son guitariste" } ] },
    "3-6" : { dir : "F" , defs : [{ target : "2-6-W", text : "Ont des instruments pouvant mimer la guitare" } ] },
    "8-2" : { dir : "H" , defs : [{ target : "8-1-N", text : "Adverbe" } , { target : "8-3-S", text : "Un des plus vieux groupe de thrash" } ] },
    "6-5" : { dir : "H" , defs : [{ target : "7-5-E", text : "Ecole de maîtres" } , { target : "7-5-S", text : "Peut diffuser du métal" } ] },
    "5-5" : { dir : "H" , defs : [{ target : "5-4-N", text : "En Albanie" } , { target : "5-6-S", text : "Jeu" } ] },
    "0-4" : { dir : "H" , defs : [{ target : "1-4-E", text : "The Voice" } , { target : "0-5-S", text : "Permet d'enregistrer du métal" } ] },
    "0-0" : { dir : "H" , defs : [{ target : "1-0-E", text : "N°1" } , { target : "1-0-S", text : "N°1 pour d'autres" } ] },
    "3-5" : { dir : "F" , defs : [{ target : "2-5-W", text : "Utiles pour le cul de chouette" } ] },
    }